<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">


| Laravel Version | PHP Version | 
| -------- | -------- | 
| 10.17.1     | 8.1.2| 


</p>

## About API
* This API is a CRUD of products
* Usage example: 
* GET: http://127.0.0.1:8000/api/products
* POST: http://127.0.0.1:8000/api/products
```
{
    "name":"Ipad",
    "description": "new product",
    "price": "9"
}
```
* PUT: http://127.0.0.1:8000/api/products/1
```
{
    "name":"Ipad",
    "description": "Update product",
    "price": "9"
}
```
* DELETE: http://127.0.0.1:8000/api/products/13

## How to
* git clone https://gitlab.com/kekosoftware/api-rest-laravel10.git
* composer install
* configure .env file
* php artisan key:generate
* php artisan migrate
* php artisan serve
* http://127.0.0.1:8000/api/products


## License
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
This API is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).